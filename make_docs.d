#!/usr/bin/env rdmd
import std;

alias f = format;

void shell(string cmd)
{
  auto result = executeShell(cmd);
  enforce(result.status == 0, f!"command %s failed: %s"(cmd, result.output));
}

void rmdirIfExists(string dir)
{
  if (exists(dir))
    rmdirRecurse(dir);
}

void main()
{
  rmdirIfExists("docs");
  mkdir("docs");

  // LibreELEC
  // Rpi
  // Android
  // TV
  // TVRemoteApp
  // Mobile
  // MediaSupport
  // 
  // Youtube
  // RegionalContent
  // Region_Belgium
  // Language_English
  // Language_Dutch
  // Language_French

  shell("template-engine install.md.template docs/install-rpi.md domain=home-theater.example.com,baseurl=https://home-theater.example.com,password=example,LibreELEC,Rpi,TV,TVRemoteApp,MediaSupport,Youtube,RegionalContent,Region_Belgium,Language_Dutch");
  shell("template-engine install.md.template docs/install-android.md domain=home-theater.example.com,baseurl=https://home-theater.example.com,password=example,Android,Mobile,Region_Belgium,Language_Dutch");

  foreach (file; dirEntries("docs", SpanMode.shallow).map!(entry => entry.name))
  {
    assert(file.endsWith(".md"));
    shell(f!"pandoc -s -f markdown %s -o %s"(file, file[0 .. $ - 2] ~ "html"));
    remove(file);
  }
}
